import { combineReducers } from "redux";
import counterReducers from './CounterReducers'

const allReducer=combineReducers({
    counterReducers
})
export default allReducer