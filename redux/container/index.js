import { connect } from 'react-redux'
import CounterComponent from '../../redux/component'
import {actionDecrement,actionIncrement}from'../../redux/action/index'



const mapStateToProps = (state) => ({
    times: state.counterReducers ? state.counterReducers : 0
})
  
const mapDispatchToProps = (dispatch) => ({
 onDecrement: () => {                        
            dispatch(actionDecrement());
        },
        onIncrement: () => {                        
            dispatch(actionIncrement());
        }
});


const CounterContainer = connect(mapStateToProps, mapDispatchToProps)(CounterComponent)
export default CounterContainer
