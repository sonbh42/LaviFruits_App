import React, { Component } from 'react'
import { Text, View } from 'react-native'

import { Provider } from 'react-redux'
import createSagaMiddleware from 'redux-saga'
import rootSaga from '../redux/saga/rootSaga'

import { createStore,applyMiddleware } from 'redux'
import CounterContainer from './container/index'

import  allReducer  from './reducer/index';
 const sagaMiddleware =createSagaMiddleware();
let store = createStore(allReducer,applyMiddleware(sagaMiddleware));


 class App extends Component {
  render() {
    return (
      <Provider store={store} >
          <CounterContainer/>
      </Provider>
    )
  }
} sagaMiddleware.run(rootSaga);
export default App