import {INCREMENT,DECREMENT} from './actionType'

export const actionIncrement = (step) => ({
    type: INCREMENT,
    step
})
export const actionDecrement = (step) => ({
    type: DECREMENT,
    step
})