import {delay} from 'redux-saga'
import {put, takeEvery} from 'redux-saga/effects'
import {INCREMENT,DECREMENT} from '../../redux/action/actionType'

function* increment(){
    console.log ('This is Increment')
}
export  function* watchIncrement(){
    yield takeEvery(INCREMENT,increment)
}
function* decrement(){
    console.log('This is decremrnt')
}
export  function*watchDecremenr(){
    yield takeEvery(DECREMENT,decrement)
}