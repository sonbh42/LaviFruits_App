import {delay} from 'redux-saga'
import {put, all} from 'redux-saga/effects'

import {watchDecremenr,watchIncrement} from './counterSaga'
export default function* rootSaga(){
    yield all ([
        watchDecremenr(),
        watchIncrement()
    ]);
}