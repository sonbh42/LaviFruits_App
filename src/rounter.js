import React, { Component } from 'react'
import { Text, View ,Image} from 'react-native'
import {createBottomTabNavigator,createStackNavigator}from 'react-navigation'
import {Icon} from 'native-base'


import Ecommerce from '../src/container/Ecommerce'
import Category from '../src/container/Category'
import Cart from './container/Cart'
import Favorite from './container/Favorite'
import Profile from './container/Profile'
import Question from './container/Question';
import Point from './container/Point'
import Help from './container/Help'
import Notification from './container/Notification'
import Account from './container/Account'
import Order from './container/Order'
import { YellowBox } from 'react-native';
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);




const Home = createBottomTabNavigator({
    Ecommerce:{
        screen: Ecommerce,

        
    },
    Category :{
        screen:Category,
        navigationOptions:{
            title :'tgggggdfv'
        }
    },
    Cart :{
        screen :Cart,
      
    },
    Favorite :{
        screen :Favorite,
        navigationOptions:{
            
            
        }
    },
    Profile:{
        screen :Profile,
     
    }
   
},{
    tabBarPosition:'bottom',
    swipeEnabled:false,
    
    tabBarOptions:{
        indicatorStyle:{height:0},
        showIcon:true,
        labelStyle  :{
            fontSize :9 ,
            width: 80
        },
        style:{
            // backgroundColor :'white',
            height:60,
            
        }
    },
    
})
const AppStack = createStackNavigator({
    Home :{screen :Home},
    Question :{screen:Question},
    Notification: {screen:Notification},
    Order:{screen:Order},
    Account : {screen :Account},
    Point:{screen:Point},
    Help :{screen: Help}

},{
    navigationOptions :{
        header :null
    }
})
export default AppStack;