import variable from './../variables'
export default (variables = variable)=> {
    const textTheme= {
        color: variables.textColor,
        ".size9":{
            fontSize: 9,
        }
    };
    return textTheme
}