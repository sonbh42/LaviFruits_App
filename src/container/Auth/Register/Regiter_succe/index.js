import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity } from 'react-native'
import { Container, Icon } from 'native-base'
import index from '../../../../assets/index'
import AuthButton from '../../../../component/AuthButton'

class Register_success extends Component {
    render() {
        return (
            <Container style={{ flex: 1, padding: 40 }}>
                <TouchableOpacity style ={{flexDirection: 'row',alignItems:'center'}}>
                    <Icon name ='ios-arrow-back-outline'/>
                    <Text style ={{marginLeft :12}}>Quay lại</Text>
                </TouchableOpacity>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>

                    <Image style={{
                        width: 250,
                        height: 100,
                        resizeMode: 'contain'
                    }} source={index.header_Lavi} ></Image>
                </View>
                <View style={{ flex: 1 }}>
                    <Text>Chúc mừng bạn đã đăng ký tài khoản thành công trên Lavifruits
                        tài khoản được dùng để đăng nhập trên app hoặc website
                    </Text>
                </View>
                <View style={{ flex: 1, justifyContent: 'center' }}>
                    <AuthButton
                        text="Xem sản phẩm" />
                </View>
                
            </Container>
        )
    }
}

export default Register_success