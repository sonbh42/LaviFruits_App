import React, { Component } from 'react'
import { Text, View, Image, Dimensions } from 'react-native'
import { Container, Content } from 'native-base'
import AuthInput from '../../component/AuthInput'
import AuthButton from '../../component/AuthButton'
import index from '../../assets/index'

export class Register extends Component {
    render() {

        return (
            <Container style={{ flex: 1, padding: 30 }}>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Image style={{
                        width: 250,
                        height: 100,
                        resizeMode: 'contain'
                    }} source={index.header_Lavi} ></Image>
                    <Text>Điền đầy đủ thông tin dưới đây</Text>
                </View>
                <View style={{ flex: 1, justifyContent: 'space-between', marginTop:12  }}>
                    <Content showsVerticalScrollIndicator={false}>
                        <AuthInput
                            placeholder='Số điện thoại'
                            keyboardType='phone-pad'
                        />
                        <AuthInput
                            placeholder='Email'
                            keyboardType='email-address'
                        />
                        <AuthInput
                            placeholder='Địa chỉ liên hệ'
                            keyboardType='default'
                        />
                        <AuthInput
                            placeholder='Mật khẩu'
                            secureTextEntry={true}

                        />
                        <AuthInput
                            placeholder='Nhắc lại mật khẩu'
                        />

                    </Content>

                    <AuthButton
                        text='Đăng ký'
                    />

                </View>

            </Container>
        )
    }
}

export default Register