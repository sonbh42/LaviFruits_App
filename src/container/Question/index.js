import React, { Component } from 'react'
import { Text, View,StyleSheet } from 'react-native'
import { Container, Content, Accordion, ListItem } from 'native-base'

import Toolbar from '../../component/Toolbar/index'
import dataArray from '../../component/Question'


 class Question extends Component {
  render() {
    return (
      <Container>
          <Toolbar title = 'Hỏi đáp'/>
          <ListItem>
              <Text style= {styles.text}>Các câu hỏi thường gặp</Text>
          </ListItem>
          <Content>
              
              <Accordion dataArray = {dataArray} />
          </Content>
      </Container>
    )
  }
}
const styles = StyleSheet.create({
    text:{
        fontSize: 20,
        color:'#222222'
    }
})
export default Question