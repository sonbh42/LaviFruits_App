import React, { Component } from 'react'
import { Text, View } from 'react-native'
import {Icon, Container} from 'native-base'
import Toolbar from '../../component/Toolbar';

 class Favorite extends Component {
  static navigationOptions ={
    tabBarLabel:'Yêu thích',
    tabBarIcon :({tintColor}) =>(
      <Icon name = "ios-heart-outline"/>
    )
    
  };
  render() {
    return (
      <Container>
        <Toolbar title='Yêu thích'/>
      </Container>
    )
  }
}

export default Favorite