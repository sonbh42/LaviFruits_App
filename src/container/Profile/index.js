import React, { Component } from 'react'
import { Text, View, StyleSheet } from 'react-native'
import { Icon, Container, ListItem, Content, Separator } from 'native-base'

import Toolbar from '../../component/Toolbar'
import MainMenu from '../../component/MenuItem/index'
import index from '../../assets/index'

export class Profile extends Component {
    static navigationOptions = {
        tabBarLabel: 'Cá nhân',
        tabBarIcon: ({ tintColor }) => (
            <Icon name="ios-contact-outline" />
        )
    };
    render() {
        return (
            <Container>
                <Toolbar title='Cá nhân' />
                <Content >

                    <MainMenu
                        onPress={() => this.props.navigation.navigate('Account')}
                        source={index.wallet} content='Tài khoản' right='ios-arrow-forward-outline' />
                    <Separator style={{ height: 10 }} />
                    <MainMenu
                        onPress={() => this.props.navigation.navigate('Notification')}
                        source={index.noti} content='Thông báo của tôi' right='ios-arrow-forward-outline' />
                    <MainMenu
                        onPress={() => this.props.navigation.navigate('Order')}
                        source={index.wallet} content='Đơn hàng của tôi' right='ios-arrow-forward-outline' />
                    <MainMenu
                        onPress={() => this.props.navigation.navigate('Point')}
                        source={index.package} content='Điểm tích lũy' right='ios-arrow-forward-outline' />
                    <Separator style={{ height: 10 }} />
                    <MainMenu
                        onPress={() => this.props.navigation.navigate('Question')}
                        source={index.wallet} content='Câu hỏi thường gặp' right='ios-arrow-forward-outline' />
                    <MainMenu
                        onPress={() => this.props.navigation.navigate('Help')}
                        source={index.wallet} content='Hỗ trợ trực tuyến' right='ios-arrow-forward-outline' />
                    <MainMenu
                        onPress={() => this.props.navigation.navigate('Question')}
                        source={index.logout} content='Đăng xuất' right='ios-arrow-forward-outline' />

                </Content>
            </Container>
        )
    }

}


export default Profile
