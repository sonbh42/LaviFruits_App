import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { Container,Header,Tabs,Tab,TabHeading } from 'native-base'


import Toolbar from '../../component/Toolbar'
import Tab1 from '../Noti_all/index'
import Tab2 from '../Noti_message/index'
import Tab3 from '../Noti_Order'


 class Notification extends Component {
  render() {
    return (
      <Container>
        <Toolbar title= "Thông báo"/>
    
       <Tabs  >
        
        <Tab  heading={ <TabHeading activeTextStyle ={{backgroundColor:'red'}} style ={{backgroundColor:'#55ac21'}}><Text>Tất cả</Text></TabHeading>}>
            <Tab1/>
          </Tab>
          <Tab heading={ <TabHeading style ={{backgroundColor:'#55ac21'}}><Text>Tin khuyến mãi</Text></TabHeading>}>
            <Tab1/>
          </Tab>
          <Tab heading={ <TabHeading style ={{backgroundColor:'#55ac21'}}><Text>Đơn hàng</Text></TabHeading>}>
            <Tab1/>
          </Tab>
      </Tabs>
      </Container>
    )
  }
}

export default Notification