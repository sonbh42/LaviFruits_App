import React, { Component } from 'react'
import { Text, View } from 'react-native'
import {Icon, Container} from 'native-base'
import Toolbar from '../../component/Toolbar';

 class Cart extends Component {
  static navigationOptions ={
    tabBarLabel:'Giỏ hàng',
    tabBarIcon :({tintColor}) =>(
      <Icon name = "ios-cart-outline"/>
    )
    
  };
  render() {
    return (
      <Container>
        <Toolbar title={'Giỏ hàng'} > </Toolbar>
      </Container>
    )
  }
}

export default Cart