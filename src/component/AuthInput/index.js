import React, { Component } from 'react'
import { Text, View, TextInput, StyleSheet } from 'react-native'


class AuthInput extends Component {
    render() {
        return (
            <View style={styles.conatainer}>
                <TextInput
                    style={styles.textInput}
                    placeholder={this.props.placeholder}
                    underlineColorAndroid="transparent"
                    keyboardType={this.props.keyboardType}
                    selectionColor="#000000"
                />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    conatainer: {
        justifyContent: 'center',
        borderBottomColor: "black",
        borderBottomWidth: 1,
    },
    textInput: {

        paddingTop: 15,
        paddingBottom: 15,
        justifyContent: "center",
        alignItems: "center",
        color:"#000000",
        paddingVertical: 0,
        fontSize: 12,

    }
})
export default AuthInput
