import React, { Component } from 'react'
import { Text, View, TouchableOpacity ,StyleSheet} from 'react-native'

class AuthButton extends Component {
    render() {
        return (
            <TouchableOpacity style={styles.container}>
                <Text style ={styles.text}>{this.props.text}</Text>
            </TouchableOpacity>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        borderRadius: 10,
        padding: 15,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor :'#55ac21'
    },
    text :{
        fontSize: 13,
        color :'white'
    }

})
export default AuthButton