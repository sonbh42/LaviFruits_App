import React, { Component } from 'react'
import { Text, View, Image } from 'react-native'
import { ListItem, Left, Body, Right, Icon } from 'native-base'
import index from '../../assets/index'

class MainMenu extends Component {
  static defaultProps = {
    content: '',
    source: index.wallet,

  }
  render() {
    return (
      <ListItem onPress = {this.props.onPress}>
        <Left>
          <Image source={this.props.source}
            style={{ width: 25, height: 25 }}
            resizeMode='contain'
          />
          <Text style ={{marginLeft:12}} >{this.props.content}</Text>
        </Left>

        <Right>
          <Icon name={this.props.right} ></Icon>
        </Right>
      </ListItem>
    )
  }
}

export default MainMenu