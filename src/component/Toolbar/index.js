
import React, { Component } from 'react'
import { Text, View, StyleSheet } from 'react-native'

import variables from '../../theme/variables'

class Toolbar extends Component {

    render() {
        return (
            <View style={styles.toolbar}>
                <View style={styles.toolbarContent}>
                    <Text style={styles.toolbarText}>
                        {this.props.title}
                    </Text>

                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    toolbar: {
        backgroundColor: variables.toolbarBackground,
        height: variables.toolbarHeight
    },
    toolbarContent: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    toolbarText: {
        color: 'white',
        fontSize: 14,
    }
})
export default Toolbar